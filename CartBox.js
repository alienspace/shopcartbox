import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { Col, Image } from 'react-bootstrap';

/** COMPONENTS CUSTOM **/
import { nextStorage } from '../../../util/nextStorage';
import { TOOLS } from '../../../util/tools.global';
export class CartBox extends Component{
  constructor(props){
    super(props);

    this.removeItem = this.removeItem.bind(this);

    this.renderProductsList = this.renderProductsList.bind(this);
    this.renderSubTotal = this.renderSubTotal.bind(this);
    this.renderCart = this.renderCart.bind(this);
  }

  componentWillReceiveProps(nextProps){
    // console.log('CartBox componentWillReceiveProps -> ',nextProps);
  }

  componentsDidMount(){
    // console.log('componentsDidMount');
  }//componentsDidMount();

  componentWillMount(){
    // console.log('componentWillMount');
  }//componentWillMount();

  removeItem(e){
    e.preventDefault();
    let
      nextDash = nextStorage.getJSON('nextDash'),
      _id = e.target.dataset._id;
    if(nextDash.CART_productsList.length > 0){
      // eslint-disable-next-line
      return nextDash.CART_productsList.map((product, key) => {
        // eslint-disable-next-line
        if(product.productID === _id){
          console.log('removed _id3 -> ',_id);
          TOOLS.removeArrayItem(nextDash.CART_productsList,key,1);
        }

        nextStorage.set('nextDash',nextDash);
        let currentPath = browserHistory.getCurrentLocation().pathname;
        browserHistory.push({pathname: currentPath});
      });
    }
  }//removeItem(e);

  renderProductsList(){
    let nextDash = nextStorage.getJSON('nextDash');
    if(nextDash.CART_productsList.length > 0){
      // console.log('nextDash.CART_productsList -> ',nextDash.CART_productsList);
      return nextDash.CART_productsList.map((product) => {
        return (
          <li key={product.productID}>
            <Link to={'#removeItem'} onClick={this.removeItem} data-_id={product.productID} className={'close fa fa-trash'}></Link>
            <Link to={'/product/'+product.productSlug} alt={product.title}>
              {product.productPhotoPrimary ?
                <figure><Image src={product.productPhotoPrimary} className="img-responsive" /></figure>
              :
                <figure><Image src={'assets/images/facebug.png'} className="img-responsive" /></figure>
              }
              <span className={'title'}>{product.productTitle}</span>
              <span className={'qty'}>{product.productQty}x</span>
              <span className={'price'}>{product.productPrice}</span>
            </Link>
          </li>
        );
      });
    }else{
      return (<li className={'empty'}>Nenhum produto na lista. :(</li>);
    }
  }//renderProductsList();

  renderSubTotal(){
    let
      nextDash = nextStorage.getJSON('nextDash'),
      subtotal = 0.0,
      subtotalProduct = 0.0;

    // eslint-disable-next-line
    nextDash.CART_productsList.map((product) => {
      /*
      console.log('subtotal BEFORE', subtotal);

      console.log('price original -> ', product.productPrice);
      console.log('price getMoney -> ', TOOLS.getMoney(product.productPrice));

      console.log('qty -> ', product.productQty);
      */

      // eslint-disable-next-line
      subtotalProduct = (parseInt(product.productQty) * TOOLS.getMoney(product.productPrice));
      /*
      console.log('subtotalProduct -> ',subtotalProduct);
      console.log('subtotal -> ',subtotal);
      */

      subtotal += + subtotalProduct;
      // console.log('subtotal -> ',subtotal);

      // eslint-disable-next-line
      /*
      console.log('product1 qty -> ',parseInt(product.productQty));
      console.log('product1 -> ',product.productPrice);
      console.log('product2 -> ',TOOLS.formatReal(TOOLS.getMoney(product.productPrice)));
      console.log('product3 -> ',TOOLS.getMoney(product.productPrice));
      console.log('subtotal AFTER', TOOLS.formatReal(subtotal));
      */
    });

    return (
      <li className={'subtotal'}>
        Sub-total <span className={'price'}>R$ {TOOLS.formatReal(subtotal)}</span>
      </li>
    );
  }//renderSubTotal();

  renderCart(){
    if(this.props.cartListHover){
      return (
        <div className={'cartListHover'}>
          <h2>Seu Carrinho</h2>
          <ul>
            {this.renderProductsList()}
            {this.renderSubTotal()}
          </ul>
          <Link
            to={'/carrinho'}
            className={'btn btn-primary no-radius pull-right'}
            >
            <span>Finalizar</span>
          </Link>
        </div>
      );
    }else if(this.props.shopCart){
      return (
        <Col xs={12} md={12} className={'no-padding'}>
          <Col xs={12} md={9} className={'no-paddingTop no-paddingLeft'}>
            <Col xs={12} md={12} className={'cartListHover active shopCart no-padding'}>
              <h2>Seu Carrinho</h2>
              <ul>
                {this.renderProductsList()}
                {this.renderSubTotal()}
              </ul>
              <Link
                to={'/'}
                className={'btn btn-secondary no-radius pull-left'}
                >
                <span>Continuar Comprando</span>
              </Link>
              <Link
                to={'/carrinho'}
                className={'btn btn-primary no-radius pull-right'}
                >
                <span>Finalizar</span>
              </Link>
            </Col>
          </Col>
          <Col xs={12} md={3} className={'cartResume cartListHover active no-padding'}>
            <h2>Resumo do seu carrinho</h2>
            <ul>
              {this.renderSubTotal()}
            </ul>
            <Link
              to={'/carrinho'}
              className={'btn btn-primary no-radius pull-right'}
              >
              <span>Finalizar</span>
            </Link>
          </Col>
        </Col>
      );
    }else{
      return (
        <Col xs={12} md={12} className={'cartListHover'}>
          <h2>Seu Carrinho</h2>
          <ul>
            {this.renderProductsList()}
            {this.renderSubTotal()}
          </ul>
          <Link
            to={'/carrinho'}
            className={'btn btn-primary no-radius pull-right'}
            >
            <span>Finalizar</span>
          </Link>
        </Col>
      );
    }
  }//renderCart();

  render(){
    return (
      <div>{this.renderCart()}</div>
    );
  }
}
